# Projeto Carro-Motorista - Ex5 da lista

### Exercício 
- **`Carro`** e **`Motorista`**: crie uma classe **`Motorista`** que tem um atributo **`carro`** e um método **`dirigir`** que usa o objeto **`Carro`** para dirigir. 

b) Sua classe `carro` deve conter um método `buzinar` e ser chamado na classe `carro`. 



## Getting started
- Clone o projeto e estude em seu computador
- Faça algumas alterações e teste, assim que a gente aprende
- Atenção:
  - Este é um projeto MAVEN
  - Esta usando o java 11. Caso queira alterar basta trocar no arquivo pom.xml
- Finalmente, tente melhorá-lo e criar algumas funcionalidades na função main.