package com.oo.projetomotoristacarro;

import com.oo.projetomotoristacarro.classes.Carro;
import com.oo.projetomotoristacarro.classes.Motorista;
import java.util.Scanner;

/**
 *
 * @author jose
 */
public class ProjetoMotoristaCarro {

    public static void main(String[] args) {
        
        // cria um objeto Carro
        Carro meuCarro = new Carro("Fusca", 1970, "verde");

        // cria um objeto Motorista e add o objeto Carro criado anteriormente
        Motorista motorista = new Motorista();
        motorista.setNome("Jose Rui");
        motorista.setCarro(meuCarro);

        // chama o método dirigir do objeto Motorista que por sua vez chamará o método buzinar da classe carro
        motorista.dirigir();
        motorista.imprimir();
        
        
        //outra opcao é atraves dos metodos preencher
        Scanner scanner = new Scanner(System.in);
        Motorista motorista2 = criarPreencherMotorista(scanner);
        motorista2.dirigir();
        motorista2.imprimir();
    }
    
    
    public static Motorista criarPreencherMotorista(Scanner scanner) {
        System.out.println("------ Preenchendo o motorista -----");

        System.out.print("Nome:");
        String nome = scanner.next();
        
        Carro carro = criarPreencherCarro(scanner);
        
        Motorista motorista = new Motorista(nome, carro);
        return motorista;
    }
    
     public static Carro criarPreencherCarro(Scanner scanner) {
        System.out.println("------ Preenchendo os dados do Carro -----");

        System.out.print("Modelo:");
        String modelo = scanner.next();

        System.out.print("Ano de fabricacao:");
        int ano = scanner.nextInt();

        System.out.print("Cor:");
        String cor = scanner.next();
        
        Carro carro = new Carro(modelo, ano, cor);
        return carro;
    }
}
